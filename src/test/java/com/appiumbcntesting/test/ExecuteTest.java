package com.appiumbcntesting.test;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExecuteTest {

    private static IOSDriver<IOSElement> driver;

    @BeforeAll
    public static void setUp() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION,"1.0");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"iOS");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"XCUITest");
        capabilities.setCapability("platformVersion","10.3");
        capabilities.setCapability(MobileCapabilityType.APP,"/Users/user/Downloads/ios-uicatalog-master/build/Release-iphoneos/UICatalog.app");
        //capabilities.setCapability("udid","");
        //capabilities.setCapability("xcodeOrgId","PRHY2VVGUL");
        //capabilities.setCapability("xcodeSigningId", "iPhone Developer");
        capabilities.setCapability("deviceName","iPhone SE");
        try {
            driver = new IOSDriver<IOSElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void tearDown() {

    }

    @AfterAll
    public static void quit() {
        driver.quit();
    }

    @Test
    public void pickerViewTest() {

    }


    @Test
    public void progressViewsTest() {

    }

    @Test
    public void slidersTest() {


    }

}
